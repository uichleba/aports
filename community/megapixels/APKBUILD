# Contributor: Martijn Braam <martijn@brixit.nl>
# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=megapixels
pkgver=1.8.1
pkgrel=0
pkgdesc="GTK+4 camera app for mobile devices"
url="https://gitlab.com/megapixels-org/Megapixels"
# s390x: doubt anyone would use a mainframe to take photosj
arch="all !s390x"
license="GPL-3.0-only"
# Required by postprocess.sh
# depends="cmd:dcraw_emu cmd:convert cmd:exiftool"
depends="libraw-tools imagemagick exiftool"
makedepends="
	feedbackd-dev
	glib-dev
	gtk4.0-dev
	libxrandr-dev
	meson
	tiff-dev>=4.6.0t
	zbar-dev
	"
subpackages="$pkgname-tools"
source="https://gitlab.com/megapixels-org/Megapixels/-/archive/$pkgver/Megapixels-$pkgver.tar.bz2
	0001-Don-t-pass-length-parameter-for-TIFFSetField-TIFFTAG.patch
	"
options="!check" # There's no testsuite
builddir="$srcdir/Megapixels-$pkgver"

build() {
	abuild-meson . output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

tools() {
	pkgdesc="Extra megapixels utilities for debugging"
	depends=""

	amove usr/bin/megapixels-list-devices
	amove usr/bin/megapixels-camera-test
}

sha512sums="
2a90198f7681233fe2a633b63ceab4cb64670c5e17eef747d7e2627aa0a1746c4a22c178e8e4eb3b21f1bfbc2dc0e1ded39a29203467e8b221b319534b511351  Megapixels-1.8.1.tar.bz2
bfaa5b1729642f2eeb5b9e6b0c3b8b6152ecd8c3510301588719efb303d27d64b27aebe0773bf7b71f6481ee4508204cbb68b5cc4b2856fb3bda60bd306f9751  0001-Don-t-pass-length-parameter-for-TIFFSetField-TIFFTAG.patch
"
