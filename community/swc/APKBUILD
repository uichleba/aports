# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swc
pkgver=1.5.0
pkgrel=0
pkgdesc="A super-fast TypeScript / JavaScript compiler written in Rust"
url="https://swc.rs"
# riscv64: it would take eternity to build
arch="all !riscv64"
license="Apache-2.0"
makedepends="cargo cargo-auditable"
source="https://github.com/swc-project/swc/archive/v$pkgver/swc-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/bindings"
# !check: TODO: run tests
# net: fetch dependencies
options="!check net"

prepare() {
	default_prepare

	# This is unwanted and breaks build on ARM.
	rm ../.cargo/config.toml

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build -p swc_cli --release --locked
}

package() {
	install -D -m755 target/release/swc -t "$pkgdir"/usr/bin/
}

sha512sums="
6aafd15243a9355df8aed29345c0283dfa1c783e6105288d668390ffafb560c234cf6660c69deb295a4591241cd8172127351545e4f10c9978b980573592323c  swc-1.5.0.tar.gz
"
